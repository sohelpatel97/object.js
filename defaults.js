const DefaultObject = (testObject,ExpectedObj)=>{
    let testkeys = []
    let ExpectedKeys = []

    for( let key in testObject){

        testkeys.push(key)
    }
    for( let key in ExpectedObj){

        ExpectedKeys.push(key)
    }
    
     //extract keys from testobj
    
     //extract keys from Expectedobj

    let commonKeys = testkeys.filter(value => ExpectedKeys.includes(value)); //common keys

    for(let k of commonKeys){

        testObject [k] = ExpectedObj [k]
        
    }
    
    return testObject


}

module.exports = DefaultObject