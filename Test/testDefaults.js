const DefaultObject = require('../defaults');

const testObject = { name: 'Bruce Wayne', age: 36, 'location': 'Gotham', partner : "CatWomen" }; 

const ExpectedObj = { name: "Batman",partner:"Robin",age:39}

//before // { name: 'Bruce Wayne', age: 36, 'location': 'Gotham', partner : "CatWomen" }

const result = DefaultObject(testObject,ExpectedObj)

//after // { name: 'Batman', age: 39, location: 'Gotham', partner: 'Robin' }

console.log(result);