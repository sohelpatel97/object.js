const pairs = require('../pairs');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; 

const res = pairs(testObject);

console.log(res) //[ [ 'name', 'Bruce Wayne' ], [ 'age', 36 ], [ 'location', 'Gotham' ] ]