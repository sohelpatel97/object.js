const mapObject = require('../mapObject');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; 

const res = mapObject(testObject,cb=(Values)=>Values+" "+"DC_UNIVERSE");

console.log(res) 
/* {
    name: 'Bruce Wayne DC_UNIVERSE',
    age: '36 DC_UNIVERSE',
    location: 'Gotham DC_UNIVERSE'
}*/

const testObject1 = {tomCruise:172, RobertDwayneJr:174, chrisHemsworth:196, DainelCraig:180 }; 

const res1 = mapObject(testObject1,cb=(Values)=>Values*2);

console.log(res1) 

/*{
  tomCruise: 344,
  RobertDwayneJr: 348,
  chrisHemsworth: 392,
  DainelCraig: 360
} 
*/