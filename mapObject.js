const mapObject = (testObject,callback)=>{
    
    let newObject = {}

    for(let key in testObject){

     newObject[key] = callback(testObject[key])

    }
    return newObject


}

module.exports = mapObject