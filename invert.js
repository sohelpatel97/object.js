const invert = (testObject)=>{
    
    let newObject = {}

    for(let prop in testObject){

        newObject[JSON.stringify(testObject[prop])] = prop
        
    }
    
    return newObject


}

module.exports = invert